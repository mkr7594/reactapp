import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

export default class App extends React.Component {
	state = {
        city: ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'Nov', 'Dec']
    }
	render() {
  	    return(
            <React.Fragment>
                <Autocomplete
                    id="combo-box-demo"
                    autoComplete={true}
                    options={this.state.data}
                    getOptionLabel={option => option}
                    style={{ width: 200}}
                    renderInput={params => <TextField {...params} label="Months" variant="outlined" />}
                />
            </React.Fragment>
    )
  }
}