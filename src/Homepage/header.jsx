import React from 'react';
import Logo from "./logo.png"
import {Link} from 'react-router-dom';

export default class Header extends React.Component {
    render() {
        return (
            <React.Fragment>
                <nav class="navbar navbar-expand-sm head-color">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <Link class="nav-link"><img src={Logo} alt="Logo"></img></Link>
                        </li>
                        <li class="nav-item">
                            <Link class="nav-link" href="#">BUS TICKETS</Link>
                        </li>
                        <li class="nav-item">
                            <Link class="nav-link" href="#">BUS HIRE</Link>
                        </li>
                        <li class="nav-item">
                            <Link class="nav-link" href="#">PILGRIMAGES</Link>
                        </li>
                    </ul>

                </nav>
            </React.Fragment>
        )
    }
}