import React from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";

export default class Textbox extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Autocomplete
          id="combo-box-demo"
          autoComplete={true}
          options={this.props.city}
          getOptionLabel={option => option}
          style={{ width: 200}}
          renderInput={params => (
            <TextField
              {...params}
              label={this.props.label}
              variant="outlined"
              style={{ background: "white" }}
              size="medium"
            />
          )}
        />
      </React.Fragment>
    );
  }
}
