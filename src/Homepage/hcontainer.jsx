import React from 'react';
import Textbox from './autocomplete';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

export default class Container extends React.Component {
    state = {
        city: ['Chennai','Delhi','Mumbai','Bangalore','Pune','Hyderabad','Trivandrum'],
    }
    render() {
        return (
          <React.Fragment>
            <div className="background">
              <Textbox city={this.state.city} label="FROM" />
              <Textbox city={this.state.city} label="TO" />
              <TextField
                id="date"
                type="date"
                variant="outlined"
                style={{ background: "white" }}
                InputLabelProps={{
                  shrink: true
                }}
              />
              <Button
                variant="contained"
                style={{
                  height: "54.3px",
                   background: "#D84F57",
                  color: "white"
                }}
              >
                Search Buses
              </Button>
            </div>
          </React.Fragment>
        );
    }
}