import React from 'react';
import Header from './header';
import Container from './hcontainer';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.home.css";

export default class Home extends React.Component {
    render() {
        return(
            <React.Fragment>
                <Header/>
                <Container/>
            </React.Fragment>
        )
    }
}